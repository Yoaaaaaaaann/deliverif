# Deliver'IF
Ajoutez des demandes de livraison sur votre carte préférée et laissez notre application calculer l'itinéraire le plus optimisé !
Application du voyageur de commerce  pour le calcul de tournées de livreurs. 
Gestion des demandes de livraison avec interface graphique.
Création de feuilles de route pour connaître l'itinéraire à  suivre.

# Demarrer l'application
Ouvrez le projet avec IntelliJ pour exectuer simplement le jar du dossier "target"

## Auteurs 
Cette application a été réalisée par 7 étudiants de l'INSA Lyon :
- Aurélie Abraham
- Maud Andruszak
- Mathéo Joseph 
- Ludivine Kupiec 
- Juliana Martins 
- Ambre Mychalski
- Yoann Sabatier 

